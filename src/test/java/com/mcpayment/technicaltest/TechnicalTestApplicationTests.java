package com.mcpayment.technicaltest;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = TechnicalTestApplication.class)
class TechnicalTestApplicationTests {

	@Test
	void contextLoads() {
	}

}
