package com.mcpayment.technicaltest.controller;

import com.mcpayment.technicaltest.datatransferobject.MerchantDTO;
import com.mcpayment.technicaltest.domainobject.MerchantDO;
import com.mcpayment.technicaltest.domainvalue.MerchantType;
import com.mcpayment.technicaltest.exception.ConstraintViolationException;
import com.mcpayment.technicaltest.exception.EntityNotFoundException;
import com.mcpayment.technicaltest.service.merchant.MerchantService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;
import java.util.List;

public class MerchantControllerTest {

    @InjectMocks
    private MerchantController merchantController;

    @Mock
    private MerchantService merchantService;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @AfterEach
    public void tearDown() {
        Mockito.verifyNoMoreInteractions(merchantService);
    }

    @Test
    void getMerchantWithValidDataThenShouldReturnSuccess () throws EntityNotFoundException {
        //GIVEN
        Mockito.when(merchantService.find(1L))
                .thenReturn(new MerchantDO("merchant01", "merchant01pw"));

        //WHEN
        MerchantDTO actual = merchantController.getMerchant(1L);

        //THEN
        Assertions.assertEquals("merchant01", actual.getUsername());
        Mockito.verify(merchantService).find(1L);
    }

    @Test
    void createMerchantWithValidDataThenShouldReturnSuccess () throws ConstraintViolationException {
        //GIVEN
        MerchantDO mockMerchantDO = new MerchantDO("merchant01", "merchant01pw");
        Mockito.when(merchantService.create(Mockito.any()))
                .thenReturn(mockMerchantDO);

        //WHEN
        MerchantDTO actual = merchantController.createMerchant(MerchantDTO.builder()
                .setUsername("merchant01")
                .setPassword("merchant01pw")
                .build());

        //THEN
        Assertions.assertEquals("merchant01", actual.getUsername());
        Mockito.verify(merchantService).create(Mockito.any());
    }

    @Test
    void deleteMerchantWithValidDataThenShouldReturnSuccess () throws EntityNotFoundException {
        //GIVEN
        Mockito.doNothing().when(merchantService).delete(1L);

        //WHEN
        merchantController.deleteMerchant(1L);

        //THEN
        Mockito.verify(merchantService).delete(1L);
    }

    @Test
    void updateMerchantTypeWithValidDataThenShouldReturnSuccess () throws EntityNotFoundException {
        //GIVEN
        Mockito.doNothing().when(merchantService).updateMerchantType(1L, MerchantType.PREMIUM);

        //WHEN
        merchantController.updateMerchantType(1L, MerchantType.PREMIUM);

        //THEN
        Mockito.verify(merchantService).updateMerchantType(1L, MerchantType.PREMIUM);
    }

    @Test
    void findMerchantByMerchantTypeWithValidDataThenShouldReturnSuccess () {
        //GIVEN
        List<MerchantDO> mockRegularMerchants = Arrays.asList(
                new MerchantDO("merchant01", "merchant01pw"),
                new MerchantDO("merchant02", "merchant02pw"),
                new MerchantDO("merchant03", "merchant03pw"));
        Mockito.when(merchantService.find(MerchantType.REGULAR)).thenReturn(mockRegularMerchants);

        //WHEN
        List<MerchantDTO> actual = merchantController.findMerchants(MerchantType.REGULAR);

        //THEN
        Assertions.assertEquals(3, actual.size());
        Mockito.verify(merchantService).find(MerchantType.REGULAR);
    }

}
