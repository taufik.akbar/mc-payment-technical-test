package com.mcpayment.technicaltest.service;

import com.mcpayment.technicaltest.dataaccessobject.MerchantRepository;
import com.mcpayment.technicaltest.domainobject.MerchantDO;
import com.mcpayment.technicaltest.domainvalue.MerchantType;
import com.mcpayment.technicaltest.exception.ConstraintViolationException;
import com.mcpayment.technicaltest.exception.EntityNotFoundException;
import com.mcpayment.technicaltest.service.merchant.DefaultMerchantService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.dao.DataIntegrityViolationException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class DefaultMerchantServiceTest {

    @InjectMocks
    private DefaultMerchantService merchantService;

    @Mock
    private MerchantRepository merchantRepository;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @AfterEach
    public void tearDown() {
        Mockito.verifyNoMoreInteractions(merchantRepository);
    }

    @Test
    void findMerchantWithValidMerchantIdThenShouldReturnSuccess() throws EntityNotFoundException {
        //GIVEN
        MerchantDO mockMerchantDO = new MerchantDO("merchant01", "merchant01pw");
        Mockito.when(merchantRepository.findById(1L)).thenReturn(Optional.of(mockMerchantDO));

        //WHEN
        MerchantDO actual = merchantService.find(1L);

        //THEN
        Assertions.assertEquals("merchant01", actual.getUsername());
        Mockito.verify(merchantRepository).findById(1L);
    }

    @Test
    void findMerchantWithInvalidMerchantIdThenShouldReturnException() throws EntityNotFoundException {
        //GIVEN
        Mockito.when(merchantRepository.findById(1L)).thenReturn(Optional.empty());

        try {
            //WHEN
            merchantService.find(1L);
        } catch (EntityNotFoundException e) {
            //THEN
            Assertions.assertEquals("Could not find entity with id: 1", e.getMessage());
            Mockito.verify(merchantRepository).findById(1L);
        }
    }

    @Test
    void createMerchantWithValidDataThenShouldReturnSuccess() throws ConstraintViolationException {
        //GIVEN
        MerchantDO mockMerchantDO = new MerchantDO("merchant01", "merchant01pw");
        Mockito.when(merchantRepository.save(mockMerchantDO)).thenReturn(mockMerchantDO);

        //WHEN
        MerchantDO actual = merchantService.create(mockMerchantDO);

        //THEN
        Assertions.assertEquals("merchant01", actual.getUsername());
        Mockito.verify(merchantRepository).save(mockMerchantDO);
    }

    @Test
    void createMerchantWithConflictDataThenShouldReturnException() {
        //GIVEN
        MerchantDO mockMerchantDO = new MerchantDO("merchant01", "merchant01pw");
        Mockito.when(merchantRepository.save(mockMerchantDO)).thenThrow(DataIntegrityViolationException.class);

        try {
            //WHEN
            merchantService.create(mockMerchantDO);
        } catch (ConstraintViolationException e) {
            //THEN
            Assertions.assertEquals(ConstraintViolationException.class, e.getClass());
            Mockito.verify(merchantRepository).save(mockMerchantDO);
        }
    }

    @Test
    void deleteMerchantWithValidMerchantIdThenShouldReturnSuccess() throws EntityNotFoundException {
        //GIVEN
        MerchantDO mockMerchantDO = new MerchantDO("merchant01", "merchant01pw");
        Mockito.when(merchantRepository.findById(1L)).thenReturn(Optional.of(mockMerchantDO));

        //WHEN
        merchantService.delete(1L);

        //THEN
        Assertions.assertEquals(true, mockMerchantDO.getDeleted());
        Mockito.verify(merchantRepository).findById(1L);
    }

    @Test
    void deleteMerchantWithInvalidMerchantIdThenShouldReturnException() {
        //GIVEN
        Mockito.when(merchantRepository.findById(1L)).thenReturn(Optional.empty());

        try {
            //WHEN
            merchantService.delete(1L);
        } catch (EntityNotFoundException e) {
            //THEN
            Assertions.assertEquals("Could not find entity with id: 1", e.getMessage());
            Mockito.verify(merchantRepository).findById(1L);
        }
    }

    @Test
    void updateMerchantTypeWithValidMerchantIdThenShouldReturnSuccess() throws EntityNotFoundException {
        //GIVEN
        MerchantDO mockMerchantDO = new MerchantDO("merchant01", "merchant01pw");
        Mockito.when(merchantRepository.findById(1L)).thenReturn(Optional.of(mockMerchantDO));

        //WHEN
        merchantService.updateMerchantType(1L, MerchantType.PREMIUM);

        //THEN
        Assertions.assertEquals(MerchantType.PREMIUM, mockMerchantDO.getMerchantType());
        Mockito.verify(merchantRepository).findById(1L);
    }

    @Test
    void updateMerchantTypeWithInvalidMerchantIdThenShouldReturnException() {
        //GIVEN
        Mockito.when(merchantRepository.findById(1L)).thenReturn(Optional.empty());

        try {
            //WHEN
            merchantService.updateMerchantType(1L, MerchantType.PREMIUM);
        } catch (EntityNotFoundException e) {
            //THEN
            Assertions.assertEquals("Could not find entity with id: 1", e.getMessage());
            Mockito.verify(merchantRepository).findById(1L);
        }
    }

    @Test
    void findMerchantsWithValidMerchantTypeThenShouldReturnSuccess() {
        //GIVEN
        List<MerchantDO> mockMerchants = Arrays.asList(
                new MerchantDO("merchant01", "merchant01pw"),
                new MerchantDO("merchant02", "merchant02pw"),
                new MerchantDO("merchant03", "merchant03pw"));
        Mockito.when(merchantRepository.findByMerchantTypeAndDeleted(MerchantType.REGULAR, false))
                .thenReturn(mockMerchants);

        //WHEN
        List<MerchantDO> actual = merchantService.find(MerchantType.REGULAR);

        //THEN
        Assertions.assertEquals(3, actual.size());
        Mockito.verify(merchantRepository).findByMerchantTypeAndDeleted(MerchantType.REGULAR, false);
    }

    @Test
    void findMerchantWithInvalidMerchantTypeThenShouldReturnEmpty() {
        //GIVEN
        Mockito.when(merchantRepository.findByMerchantTypeAndDeleted(MerchantType.REGULAR, false)).thenReturn(new ArrayList<>());

        //WHEN
        List<MerchantDO> actual = merchantService.find(MerchantType.REGULAR);

        //THEN
        Assertions.assertEquals(0, actual.size());
        Mockito.verify(merchantRepository).findByMerchantTypeAndDeleted(MerchantType.REGULAR, false);
    }

}
