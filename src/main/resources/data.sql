/**
 * CREATE Script for init of DB
 */

-- Create 3 REGULAR merchants
insert into merchant (id, username, password, merchant_type, date_created, deleted) values (1, 'merchant001', 'merchant001pw', 'REGULAR', now(), false);
insert into merchant (id, username, password, merchant_type, date_created, deleted) values (2, 'merchant002', 'merchant002pw', 'REGULAR', now(), false);
insert into merchant (id, username, password, merchant_type, date_created, deleted) values (3, 'merchant003', 'merchant003pw', 'REGULAR', now(), false);

-- Create 2 PREMIUM merchants
insert into merchant (id, username, password, merchant_type, date_created, deleted) values (4, 'merchant004', 'merchant004pw', 'PREMIUM', now(), false);
insert into merchant (id, username, password, merchant_type, date_created, deleted) values (5, 'merchant005', 'merchant005pw', 'PREMIUM', now(), false);
