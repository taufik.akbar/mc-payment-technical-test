package com.mcpayment.technicaltest.dataaccessobject;

import com.mcpayment.technicaltest.domainobject.MerchantDO;
import com.mcpayment.technicaltest.domainvalue.MerchantType;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Database Access Object for merchant table.
 * <p/>
 */
public interface MerchantRepository extends CrudRepository<MerchantDO, Long> {
    List<MerchantDO> findByMerchantTypeAndDeleted(MerchantType merchantType, boolean deleted);
}
