package com.mcpayment.technicaltest.controller;

import com.mcpayment.technicaltest.controller.mapper.MerchantMapper;
import com.mcpayment.technicaltest.datatransferobject.MerchantDTO;
import com.mcpayment.technicaltest.domainobject.MerchantDO;
import com.mcpayment.technicaltest.domainvalue.MerchantType;
import com.mcpayment.technicaltest.exception.ConstraintViolationException;
import com.mcpayment.technicaltest.exception.EntityNotFoundException;
import com.mcpayment.technicaltest.service.merchant.MerchantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * All operations with a merchant will be routed by this controller.
 * <p/>
 */
@RestController
@RequestMapping("v1/merchants")
public class MerchantController {

    private final MerchantService merchantService;

    @Autowired
    public MerchantController(MerchantService merchantService) {
        this.merchantService = merchantService;
    }


    @GetMapping("/{merchantId}")
    public MerchantDTO getMerchant(@PathVariable long merchantId) throws EntityNotFoundException {
        return MerchantMapper.makeMerchantDTO(merchantService.find(merchantId));
    }


    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public MerchantDTO createMerchant(@Valid @RequestBody MerchantDTO merchantDTO) throws ConstraintViolationException {
        MerchantDO merchantDO = MerchantMapper.makeMerchantDO(merchantDTO);
        return MerchantMapper.makeMerchantDTO(merchantService.create(merchantDO));
    }


    @DeleteMapping("/{merchantId}")
    public void deleteMerchant(@PathVariable long merchantId) throws EntityNotFoundException {
        merchantService.delete(merchantId);
    }


    @PutMapping("/{merchantId}")
    public void updateMerchantType(
            @PathVariable long merchantId, @RequestParam MerchantType merchantType)
            throws EntityNotFoundException {
        merchantService.updateMerchantType(merchantId, merchantType);
    }


    @GetMapping
    public List<MerchantDTO> findMerchants(@RequestParam MerchantType merchantType) {
        return MerchantMapper.makeMerchantDTOList(merchantService.find(merchantType));
    }

}
