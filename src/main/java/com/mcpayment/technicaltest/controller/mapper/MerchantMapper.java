package com.mcpayment.technicaltest.controller.mapper;

import com.mcpayment.technicaltest.datatransferobject.MerchantDTO;
import com.mcpayment.technicaltest.domainobject.MerchantDO;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class MerchantMapper {

    public static MerchantDO makeMerchantDO(MerchantDTO merchantDTO) {
        return new MerchantDO(merchantDTO.getUsername(), merchantDTO.getPassword());
    }


    public static MerchantDTO makeMerchantDTO(MerchantDO merchantDO) {
        MerchantDTO.MerchantDTOBuilder merchantDTOBuilder = MerchantDTO.builder()
                .setId(merchantDO.getId())
                .setPassword(merchantDO.getPassword())
                .setUsername(merchantDO.getUsername())
                .setMerchantType(merchantDO.getMerchantType());

        return merchantDTOBuilder.build();
    }


    public static List<MerchantDTO> makeMerchantDTOList(Collection<MerchantDO> merchants) {
        return merchants.stream()
                .map(MerchantMapper::makeMerchantDTO)
                .collect(Collectors.toList());
    }

}
