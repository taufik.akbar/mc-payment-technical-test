# MC Payment Indonesia backend applicant test

## Task Description
You should be able to start the example application by executing com.mcpayment.technicaltest.TechnicalTestApplication, which starts a webserver on port 8080 (http://localhost:8080) and serves SwaggerUI where can inspect and try existing endpoints.

The project is based on a small web service which uses the following technologies:

* Java 11
* Spring Boot
* Database H2 (In-Memory)
* Maven


You should be aware of the following conventions while you are working on this exercise:

 * All new entities should have an ID with type of Long and a date_created with type of ZonedDateTime.
 * The architecture of the web service is built with the following components:
 	* DataTransferObjects: Objects which are used for outside communication via the API
    * Controller: Implements the processing logic of the web service, parsing of parameters and validation of in- and outputs.
    * Service: Implements the business logic and handles the access to the DataAccessObjects.
    * DataAccessObjects: Interface for the database. Inserts, updates, deletes and reads objects from the database.
    * DomainObjects: Functional Objects which might be persisted in the database.
 * TestDrivenDevelopment is a good choice, but it's up to you how you are testing your code.

---


## Task 1
 * Write a new Controller for maintaining transaction (CRUD).
   * Decide on your own how the methods should look like.
   * Entity Transaction: Should have at least the following characteristics: amount, payment_channel (eg: DANA, OVO, ShopeePay, LinkAja), paid_time (nullable), expired_time, and should have relation to the Merchant table.
   * Entity Payment: Should have at least the following characteristics: amount, issuer_name, and should have relation to the Transaction table.
 * Extend the MerchantController to enable merchants to create a transaction for them (one merchant could have more than one transaction).
 * Extend the TransactionController to enable user to pay the transaction.
 * Add example data to resources/data.sql
 
_NOTE: Please DO NOT publish the project, e.g. by uploading it to GitHub or the like!_

---


## Task 2
Single close payment: A transaction can be paid by exactly one payment with valid amount only!. If user tries to pay the transaction that is already paid, you should throw TransactionAlreadyPaidException. Or If user tries to pay the transaction with invalid amount, you should throw InvalidAmountException.

---


## Task 3
Imagine a transaction management frontend that is used internally by MC Payment Indonesia employees to manage merchants and transactions related data. For a new search functionality, we need an endpoint to search for merchants and transactions. 
* Implement a new endpoint for searching transactions that are paid only, return list of paid transactions.
* Implement a new endpoint for searching merchants with the highest transactions, return list of top merchants

---


## Task 4 (optional)
This task is _voluntarily_, if you can't get enough of hacking tech challenges, implement security.
Secure the API so that authentication is needed to access it. The details are up to you.

Please include instructions how to authenticate/login, so that we can test the endpoints you implemented!


---

## Submission

+ Fork it to a `private` gitlab repository (go to `Settings -> General -> Visibility, project features, permissions -> Project visibility`).
+ Share the project with gitlab user `taufik.akbar` (go to `Settings -> Members -> Invite member`, find the user in `Select members to invite` and set `Choose a role permission` to `Developer`)
+ Send us an `ssh` clone link to the repository.

---


Good luck! <br/>
❤️ <br/>
MC Payment Indonesia


_NOTE: Please make sure to not submit any personal data with your tests result. Personal data is for example your name, your birth date, email address etc._